﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WCF2
{
    [DataContract]
    public class Movie
    {

        [DataMember(IsRequired = false)]
        public string Title { get; set; }

        [DataMember(IsRequired = false)]
        public string Director { get; set; }

        [DataMember(IsRequired = false)]
        public string Genre { get; set; }

        [DataMember(IsRequired = false)]
        public int Rating { get; set; }
    }
}
